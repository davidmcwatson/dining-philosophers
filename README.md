# Dining Philosophers #

This code was written to satisfy Lab 3 of Comp301 at the University of Waikato.

The dining philosophers problem is an example problem often used in concurrent algorithm design to illustrate synchronization issues and techniques for resolving them. 
It consists of an arbitary number of philosophers (usually 5) sitting at a dining table. There are as many forks on the table as philosophers, one to the left and right of each philosopher such that they share with their neighbours. 
The problem is to devise an algorithm for synchronizing access to the forks in a way that allows the process to carry on indefinately.

In this repo is a working implementation that never blocks as well as a deadlocking implementation for comparison. 

More information about the problem can be found [here](https://en.wikipedia.org/wiki/Dining_philosophers_problem).

### How do I get set up? ###

Compilation can be done with the included Makefile. 
Targets are:

+ all (compiles both programs)
+ dining (compiles working solution)
+ dining\_deadlock (compiles deadlocking solution)
+ clean - removes all compiled executables

