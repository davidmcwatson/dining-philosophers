#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N           5
#define LEFT        (i+N-1)%N
#define RIGHT       (i+1)%N
#define THINKING    0
#define HUNGRY      1
#define EATING      2

pthread_t philosophers[N];        // One thread for each philosopher
pthread_mutex_t forks[N];         // Controls access to each fork
pthread_mutex_t test_fork_mutex;  // Allows only one thread to test the forks at once
pthread_mutex_t console_mutex;    // Controls access to the console
int pflags[N];                    // Holds state of the philosophers

void think(int i)
{
    pthread_mutex_lock(&console_mutex);
    printf("Philosopher %d is thinking\n", i);
    //sleep(rand() % 3);
    pthread_mutex_unlock(&console_mutex);
}

void eat(int i)
{
    pthread_mutex_lock(&console_mutex);
    printf("Philosopher %d is eating\n", i);
    pthread_mutex_unlock(&console_mutex);
}

void test(int i)
{
    if( pflags[i]     == HUNGRY
     && pflags[LEFT]  != EATING
     && pflags[RIGHT] != EATING)
     {
         pflags[i] = EATING;
         pthread_mutex_unlock( &forks[i] );
     }
}

// Takes forks from both the left and right of i
void take_forks(int i)
{
    pthread_mutex_lock(&test_fork_mutex);

    pflags[i] = HUNGRY;
    test(i);

    pthread_mutex_unlock(&test_fork_mutex);

    pthread_mutex_lock( &forks[i] );
}
// Puts back both forks
void put_forks(int i)
{
    pthread_mutex_lock(&test_fork_mutex);

    pflags[i] = THINKING;
    test(LEFT);
    test(RIGHT);

    pthread_mutex_unlock(&test_fork_mutex);
}

void philosopher(int i)
{
    while(1)
    {
        think(i);
        take_forks(i);
        eat(i);
        put_forks(i);
    }
}

void main()
{
    int i;

    for( i = 0; i < N; i++ ) {
        pflags[i] = THINKING;
    }
    for( i = 0; i < N; i++ ) {
        pthread_create(&philosophers[i],NULL,(void *)philosopher,(int *)i);
    }
    for( i = 0; i < N; i++ ) {
        pthread_join(philosophers[i],NULL);
    }
}
