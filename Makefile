all: dining_deadlock dining

dining_deadlock: dining_deadlock.c
	gcc -o dining_deadlock dining_deadlock.c -lpthread

dining: dining.c
	gcc -o dining dining.c -lpthread

clean:
	rm dining dining_deadlock
