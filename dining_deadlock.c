#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N 5

pthread_t philosophers[N];
pthread_mutex_t forks[N];
pthread_mutex_t console_mutex;

void think(int i)
{
    pthread_mutex_lock(&console_mutex);
    printf("Philosopher %d is thinking\n", i);
    pthread_mutex_unlock(&console_mutex);
}

void eat(int i)
{
    pthread_mutex_lock(&console_mutex);
    printf("Philosopher %d is eating\n", i);
    pthread_mutex_unlock(&console_mutex);
}

void take_fork(int i)
{
    pthread_mutex_lock(&forks[i]);
}

void put_fork(int i)
{
    pthread_mutex_unlock(&forks[i]);
}

void philosopher(int i)
{
    while(1)
    {
        think(i);
        take_fork(i);
        take_fork((i + 1) % N);
        eat(i);
        put_fork(i);
        put_fork((i+1) % N);
    }
}

void main()
{
    int i;

    for( i = 0; i < N; i++ ) {
        pthread_create(&philosophers[i],NULL,(void *)philosopher,(int *)i);
    }
    for( i = 0; i < N; i++ ) {
          pthread_join(philosophers[i],NULL);
    }
}
